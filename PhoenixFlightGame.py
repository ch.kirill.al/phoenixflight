import pygame
from random import randint
import random
import os
import sys

pygame.init()
pygame.time.set_timer(pygame.USEREVENT, 2000)

W, H = 800, 600
screen = pygame.display.set_mode((W, H), pygame.NOFRAME)

clock = pygame.time.Clock()
FPS = 60
gravity = 0.25


screen_rect = (0, 0, W, H)


def pop(level):

    f = open(level, 'r', encoding="UTF-8")
    dt = f.readlines()
    speed = dt[0].split()[2]
    game_points_for_win = dt[1].split()[2]
    lives = dt[2].split()[2]
    max_speed = dt[3].split()[2]
    return speed, game_points_for_win, lives, max_speed


background_image_start = pygame.image.load('data/img_24.png').convert()
background_image_start = pygame.transform.scale(background_image_start,
                                          (background_image_start.get_width(), background_image_start.get_height()))

f_start = pygame.font.SysFont('colibri', 35)

intro_text = ["                                             ПОЛЕТ ФЕНИКСА",
              "Правила игры: ",
              "Для победы наберите 10000 очков, собирая огоньки.",
              "Пойманные звездочки ускоряют вашего феникса,",
              "а снежинки наоборот - замедляют.",
              "Но будьте осторожны - 3 собранные капли, и вы проиграли!", "",
              "        Кликните на выбранный уровень, а затем нажмите",
              "             'Пробел' для старта. Для выхода нажмите 'v'.", ""]

text_coord = 50
for line in intro_text:
    string_rendered = f_start.render(line, 1, (255, 255, 51))
    intro_rect = string_rendered.get_rect()
    text_coord += 20
    intro_rect.top = text_coord
    intro_rect.x = 20
    text_coord += intro_rect.height
    screen.blit(background_image_start, (0, 0))
    background_image_start.blit(string_rendered, intro_rect)

sh = pygame.font.Font(None, 50)
restart_label_1 = sh.render('Просто', False, (0, 102, 51))
restart_label_1_rect = restart_label_1.get_rect(topleft=(100, 500))

restart_label_2 = sh.render('Сложно', False, (255, 128, 0))
restart_label_2_rect = restart_label_2.get_rect(topleft=(300, 500))

restart_label_3 = sh.render('Невозможно', False, (255, 0, 0))
restart_label_3_rect = restart_label_3.get_rect(topleft=(500, 500))

screen.blit(restart_label_1, restart_label_1_rect)
screen.blit(restart_label_2, restart_label_2_rect)
screen.blit(restart_label_3, restart_label_3_rect)

pygame.display.update()


def load_image(name, colorkey=None):
    fullname = os.path.join('data', name)

    # если файл не существует, то выходим
    if not os.path.isfile(fullname):
        print(f"Файл с изображением '{fullname}' не найден")
        sys.exit()
    image = pygame.image.load(fullname)

    if colorkey is not None:
        image = image.convert()
        if colorkey == -1:
            colorkey = image.get_at((0, 0))
        image.set_colorkey(colorkey)
    else:
        image = image.convert_alpha()
    return image


class Particle(pygame.sprite.Sprite):
    # сгенерируем частицы разного размера
    fire = [load_image("img_7.png")]
    fire[0].set_colorkey((0, 0, 0))
    #img = pygame.transform.scale(fire[0], (fire[0].get_width() // 10, fire[0].get_height() // 10))

    for scale in (5, 10, 20):
        fire.append(pygame.transform.scale(fire[0], (scale, scale)))

    def __init__(self, pos, dx, dy):
        super().__init__(all_sprites)
        self.image = random.choice(self.fire)
        self.rect = self.image.get_rect()

        # у каждой частицы своя скорость - это вектор
        self.velocity = [dx, dy]
        # и свои координаты
        self.rect.x, self.rect.y = pos

        # гравитация будет одинаковой
        self.gravity = gravity

    def update(self):
        # применяем гравитационный эффект:
        # движение с ускорением под действием гравитации
        self.velocity[1] += self.gravity
        # перемещаем частицу
        self.rect.x += self.velocity[0]
        self.rect.y += self.velocity[1]
        # убиваем, если частица ушла за экран
        if not self.rect.colliderect(screen_rect):
            self.kill()


def create_particles(position):
    # количество создаваемых частиц
    particle_count = 20
    # возможные скорости
    numbers = range(-5, 6)
    for _ in range(particle_count):
        Particle(position, random.choice(numbers), random.choice(numbers))


all_sprites = pygame.sprite.Group()
pygame.mouse.set_visible(0)
pygame.display.set_caption('Фон')

background_image = pygame.image.load('data/img_24.png').convert()
background_image_1 = pygame.image.load('data/img_24.png').convert()
img_points = pygame.image.load('data/img_10.png').convert()

shp = pygame.font.Font(None, 30)
restart_label = shp.render('Разработчик: К.А. Чуркин (ch.kirill.al@gmail.com)', False, (218, 165, 32))
restart_label_rect = restart_label.get_rect(topleft=(30, 570))
background_image.blit(restart_label, restart_label_rect)

fire = pygame.image.load('data/img_30.png').convert()
fire.set_colorkey((255, 255, 255))
fire = pygame.transform.scale(fire, (fire.get_width() // 5.5, fire.get_height() // 5.5))

img = pygame.image.load('data/Phonex.png').convert()
img.set_colorkey((0, 0, 0))
img = pygame.transform.scale(img, (img.get_width() // 3.5, img.get_height() // 3.5))

bomb = pygame.image.load('data/img_31.png').convert()
bomb.set_colorkey((0, 0, 0))
bomb = pygame.transform.scale(bomb, (bomb.get_width() // 4.5, bomb.get_height() // 4.5))


heart = pygame.image.load('data/img_18.png').convert()
heart.set_colorkey((255, 255, 255))
heart = pygame.transform.scale(heart, (heart.get_width() // 4.5, heart.get_height() // 4.5))

snow_up = img
snow_down = pygame.transform.flip(img, 0, 1)
snow_right = pygame.transform.rotate(img, -90)
snow_left = pygame.transform.rotate(img, 90)

snow_rect = img.get_rect(center=(W//2, H//2))


background_image = pygame.transform.scale(background_image,
                                          (background_image.get_width(), background_image.get_height()))


img_points = pygame.transform.scale(img_points,
                                          (img_points.get_width() // 2, img_points.get_height() // 2))
k = pygame.font.SysFont('arial', 50)
f = pygame.font.SysFont('colibri', 50)

line = pygame.Surface((800, 10))
line.fill((0, 255, 0))
line.set_alpha(200)

snow = snow_up

speed = 5
game_points_for_win = 10000
lives = 1

max_speed = 10
game_points = 0


class Ball(pygame.sprite.Sprite):
    def __init__(self, x, speed, surf, points, group):
        pygame.sprite.Sprite.__init__(self)
        self.image = surf
        self.rect = self.image.get_rect(center=(x, 0))
        self.speed = speed
        self.points = points
        self.add(group)

    def update(self, *args):
        if self.points == -5:
            if self.rect.x < args[0] - 200:
                self.rect.x += self.speed
                self.rect.y = 200
        elif self.rect.y < args[0] - 200:
                self.rect.y += self.speed
        else:
            self.kill()


def createBall(group):
    indx = randint(0, len(balls_surf)-1)
    x = randint(20, W-20)
    if indx == 3:
        speed = 8
    else:
        speed = randint(1, 4)

    return Ball(x, speed, balls_surf[indx], points[indx]['points'], group)

g_points = 0

def collision():
    global game_points, speed, lives, g_points
    for ob in balls:
        if snow_rect.collidepoint(ob.rect.center):
            if ob.points == 50:
                speed += 1
                if speed > max_speed:
                    speed = max_speed
                game_points += ob.points
                g_points += ob.points

                ob.kill()

            elif ob.points == -50:
                speed -= 1
                if speed < 2:
                    speed = 2
                game_points += ob.points
                g_points += ob.points
                ob.kill()

            elif ob.points == 500:
                game_points += ob.points
                g_points += ob.points
                ob.kill()

            elif ob.points == -500:
                create_particles(ob.rect.center)
                background_image.fill((0, 0, 0))
                background_image.blit(background_image_1, (0, 0))
                create()
                lives -= 1
                ob.kill()


sn = pygame.image.load('data/img_8.png').convert()
sn.set_colorkey((255, 255, 255))
sn = pygame.transform.scale(sn, (sn.get_width() // 10, sn.get_height() // 10))


star = pygame.image.load('data/img_7.png').convert()
star.set_colorkey((0, 0, 0))

block = pygame.image.load('data/Преграда.png').convert()
block.set_colorkey((0, 0, 0))

balls_surf = [star, sn, fire, bomb]


balls = pygame.sprite.Group()


points = ({'path': star, 'points': 50},
          {'path': sn, 'points': -50},
          {'path': fire, 'points': 500},
          {'path': bomb, 'points': -500})


pygame.draw.rect(screen, (0, 0, 255), (0, 0, W, H))
background_image_end = pygame.image.load('data/Game Over.png').convert()
background_image_end = pygame.transform.scale(background_image_end,
                                          (background_image_end.get_width() // 3.15, background_image_end.get_height() // 2.65))

background_image_rect = background_image_end.get_rect(center=(W//2, H//2))
all_sprites_end = pygame.sprite.Group()


background_image_rect.x = -800
background_image_rect.y = 0


pygame.draw.rect(screen, (0, 0, 255), (0, 0, W, H))
background_image_end_win = pygame.image.load('data/Win_end.png').convert()


background_image_end_win = pygame.transform.scale(background_image_end_win,
                                          (background_image_end_win.get_width() * 1.3, background_image_end_win.get_height()))


def load_image_z(name, color_key=None):
    fullname = os.path.join('data', name)
    try:
        image = pygame.image.load(fullname).convert()
    except pygame.error as message:
        print('Cannot load image:', name)
        raise SystemExit(message)

    if color_key is not None:
        if color_key == -1:
            color_key = image.get_at((0, 0))
        image.set_colorkey(color_key)
    else:
        image = image.convert_alpha()
    return image


background_image_rect = background_image_end_win.get_rect(center=(W//2, H//2))

background_image_rect.x = -800
background_image_rect.y = 0


x = randint(20, 500)
y = randint(20, 450)


def pops(level):
    f = open(level, 'r', encoding="UTF-8")
    dt = f.readlines()
    speed = dt[0].split()[2]
    game_points_for_win = dt[1].split()[2]
    lives = dt[2].split()[2]
    max_speed = dt[3].split()[2]
    return speed, game_points_for_win, lives, max_speed


block = pygame.image.load('data/Преграда.png').convert()
block = pygame.transform.scale(block, (block.get_width() // 2, block.get_height()))


def create():
    global x, y
    background_image.blit(block, (x, y))
    pygame.display.update()



flag = False
flag_end = False
pygame.mouse.set_visible(True)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()

        elif event.type == pygame.USEREVENT:
            createBall(balls)

    mouse = pygame.mouse.get_pos()

    if restart_label_1_rect.collidepoint(mouse) and pygame.mouse.get_pressed()[0]:

        speed = int(pop('easy.txt')[0])
        game_points_for_win = int(pop('easy.txt')[1])
        lives = int(pop('easy.txt')[2])
        max_speed = int(pop('easy.txt')[3])

    if restart_label_2_rect.collidepoint(mouse) and pygame.mouse.get_pressed()[0]:

        speed = int(pop('hard.txt')[0])
        game_points_for_win = int(pop('hard.txt')[1])
        lives = int(pop('hard.txt')[2])
        max_speed = int(pop('hard.txt')[3])

    if restart_label_3_rect.collidepoint(mouse) and pygame.mouse.get_pressed()[0]:

        speed = int(pop('impossible.txt')[0])
        game_points_for_win = int(pop('impossible.txt')[1])
        lives = int(pop('impossible.txt')[2])
        max_speed = int(pop('impossible.txt')[3])

    play = pygame.key.get_pressed()

    if play[pygame.K_SPACE]:
        create()

        game_points = 0
        flag = True

    if play[pygame.K_v]:
        exit()

    if flag:
        if play[pygame.K_LEFT]:
            snow = snow_left
            snow_rect.x -= speed
            if snow_rect.x < 0:
                snow_rect.x = 0

        elif play[pygame.K_RIGHT]:
            snow = snow_right
            snow_rect.x += speed
            if snow_rect.x > 650:
                snow_rect.x = 650

        elif play[pygame.K_UP]:
            snow = snow_up
            snow_rect.y -= speed
            if snow_rect.y < 0:
                snow_rect.y = 0

        elif play[pygame.K_DOWN]:
            snow = snow_down
            snow_rect.y += speed
            if snow_rect.y > 450:
                snow_rect.y = 450

        if (x <= snow_rect.x <= block.get_width() // 2 + x) and (y <= snow_rect.y <= block.get_height() + y):
            snow_rect.x = randint(20, 700)
            snow_rect.y = randint(20, 550)

        collision()

        all_sprites.draw(background_image)
        all_sprites.update()

        pygame.display.flip()

        background_image.blit(line, (0, 450))

        background_image.blit(img_points, (0, 0))

        background_image.blit(heart, (670, -15))

        screen_text = k.render(str(game_points), 1, (94, 138, 14))
        background_image.blit(screen_text, (20, 10))

        screen_text1 = f.render(str(lives), 1, (94, 138, 14))
        background_image.blit(screen_text1, (727, 40))

        screen.blit(background_image, (0, 0))
        balls.draw(screen)

        screen.blit(snow, snow_rect)
        pygame.display.update()

        if game_points >= game_points_for_win:
            while background_image_rect.x < 0:
                background_image_rect.x = background_image_rect.x + 20

                screen.blit(background_image_end_win, (background_image_rect.x, background_image_rect.y))
                all_sprites_end.draw(screen)
                pygame.display.flip()
                pygame.time.delay(100)
            exit()

        if lives <= 0:
            flag = False
            flag_end = True

    elif flag_end:
        while background_image_rect.x < 0:
            background_image_rect.x = background_image_rect.x + 20

            sh = pygame.font.Font(None, 35)
            restart_label = sh.render(f'Колличесиво набраных очков: {g_points}', False, (250, 0, 0))
            restart_label_rect = restart_label.get_rect(topleft=(230, 450))

            screen.blit(background_image_end, (background_image_rect.x, background_image_rect.y))
            all_sprites_end.draw(screen)
            pygame.display.flip()
            pygame.time.delay(100)
            background_image_end.blit(restart_label, restart_label_rect)

        background_image_start = pygame.image.load('data/img_24.png').convert()
        background_image_start = pygame.transform.scale(background_image_start,
                                                            (background_image_start.get_width(),
                                                             background_image_start.get_height()))

        f_start = pygame.font.SysFont('colibri', 35)

        intro_text = ["                                             ПОЛЕТ ФЕНИКСА",
                          "Правила игры: ",
                          "Для победы наберите 10000 очков, собирая огоньки.",
                          "Пойманные звездочки ускоряют вашего феникса,",
                          "а снежинки наоборот - замедляют.",
                          "Но будьте осторожны - 3 собранные капли, и вы проиграли!", "",
                          "        Кликните на выбранный уровень, а затем нажмите",
              "             'Пробел' для старта. Для выхода нажмите 'v'.", ""]

        text_coord = 50
        for line in intro_text:
            string_rendered = f_start.render(line, 1, (255, 255, 51))
            intro_rect = string_rendered.get_rect()
            text_coord += 20
            intro_rect.top = text_coord
            intro_rect.x = 20
            text_coord += intro_rect.height
            screen.blit(background_image_start, (0, 0))
            background_image_start.blit(string_rendered, intro_rect)

        line = pygame.Surface((800, 10))
        line.fill((0, 255, 0))
        line.set_alpha(200)

        sh = pygame.font.Font(None, 50)
        restart_label_1 = sh.render('Просто', False, (0, 102, 51))
        restart_label_1_rect = restart_label_1.get_rect(topleft=(100, 500))

        restart_label_2 = sh.render('Сложно', False, (255, 128, 0))
        restart_label_2_rect = restart_label_2.get_rect(topleft=(300, 500))

        restart_label_3 = sh.render('Невозможно', False, (255, 0, 0))
        restart_label_3_rect = restart_label_3.get_rect(topleft=(500, 500))

        screen.blit(restart_label_1, restart_label_1_rect)
        screen.blit(restart_label_2, restart_label_2_rect)
        screen.blit(restart_label_3, restart_label_3_rect)

        pygame.display.update()
        flag_end = False

    clock.tick(FPS)
    balls.update(H)


